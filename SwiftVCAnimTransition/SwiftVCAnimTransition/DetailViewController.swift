//
//  DetailViewController.swift
//  SwiftVCAnimTransition
//
//  Created by David Coufal on 8/11/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, RSViewControllerAnimationTargetProtocol {
                            
    @IBOutlet weak var headerView: RSSwVCAnimDataHeaderView!
    @IBOutlet weak var textView: UITextView!


    var detailItem: RSSwVCAnimDataItem? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail: RSSwVCAnimDataItem = self.detailItem {
            if let headerView = self.headerView {
                headerView.imageView!.image = UIImage(named: detail.imageName)
                headerView.label!.text = detail.shortText
                headerView.backgroundColor = detail.backgroundColor
            }
            if let textView = self.textView {
                textView.text = detail.longText
            }
        }
        self.title = "Fun Fact"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }
    
    override func viewDidAppear(animated: Bool) {
        if let textView = self.textView {
//            textView.scrollRectToVisible(CGRectMake(0.0, 0.0, 1.0, 1.0), animated: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - RSViewControllerAnimationTargetProtocol
    
    func acceptsSourceClass( target: String) -> Bool {
        return (target == "MasterViewController")
    }
    
    func targetClass() -> String{
        return "DetailViewController"
    }

    func targetAnimationFrame() -> CGRect{
        return self.view.convertRect(self.headerView.frame, fromView: self.view)
    }

}

