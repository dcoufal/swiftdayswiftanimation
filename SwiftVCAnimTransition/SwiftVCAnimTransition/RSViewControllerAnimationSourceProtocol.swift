//
//  RSViewControllerAnimationSourceProtocol.swift
//  SwiftVCAnimTransition
//
//  Created by David Coufal on 8/11/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

import UIKit

@objc protocol RSViewControllerAnimationSourceProtocol {
    func acceptsTargetClass( target: String) -> Bool
    func sourceClass() -> String
    func sourceView() -> UIView
    func sourceAnimationFrame() -> CGRect
    func preAnimationSetup( view: UIView, sourceViewController: UIViewController, targetViewController: UIViewController)
    func postAnimationSetup( view: UIView, sourceViewController: UIViewController, targetViewController: UIViewController)
    optional func transitionDuration( distanceTravelledAsFractionOFScreen: CGFloat) -> NSTimeInterval
    optional func transitionSpringDamping() -> CGFloat
    optional func transitionSpringInitialVelocity() -> CGFloat
    optional func willStartSourceAnimation()
    optional func didEndSourceAnimation()
}
