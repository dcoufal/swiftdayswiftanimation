//
//  RSSwVCAnimTableViewCell.swift
//  SwiftVCAnimTransition
//
//  Created by David Coufal on 8/11/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

import UIKit

class RSSwVCAnimTableViewCell: UITableViewCell {
    
    @IBOutlet var headerView: RSSwVCAnimDataHeaderView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
