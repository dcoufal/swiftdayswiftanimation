//
//  RSSwVCAnimDataHeaderView.swift
//  SwiftVCAnimTransition
//
//  Created by David Coufal on 8/11/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

import UIKit

class RSSwVCAnimDataHeaderView: UIView {
    
    var imageView: UIImageView?
    var label: UILabel?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureView()
    }
    
    required init(coder aDecoder: NSCoder!) {
        super.init(coder: aDecoder)
        self.configureView()
    }
    
    func configureView() {
        var imgFrame = CGRectMake(0.0, 0.0, self.bounds.height, self.bounds.height)
        self.imageView = UIImageView(frame: imgFrame)
        var labelFrame = CGRectMake(self.bounds.height + 5.0, 0.0, self.bounds.width - self.bounds.height, self.bounds.height)
        self.label = UILabel(frame: labelFrame)
        
        self.label!.font = UIFont(name: "Helvetica", size: 16.0)
        self.label!.textColor = UIColor.blackColor()
        self.label!.textAlignment = NSTextAlignment.Left
        self.label!.backgroundColor = UIColor.clearColor()
        
        self.addSubview(self.imageView!)
        self.addSubview(self.label!)
        
        var imgHeightConstraint = NSLayoutConstraint(item: self.imageView,
            attribute: NSLayoutAttribute.Height,
            relatedBy: NSLayoutRelation.Equal,
            toItem: self,
            attribute: NSLayoutAttribute.Height,
            multiplier: 1.0,
            constant: 1.0);
        
        var imgLeftSticky = NSLayoutConstraint(item: self.imageView,
            attribute: NSLayoutAttribute.Left,
            relatedBy: NSLayoutRelation.Equal,
            toItem: self,
            attribute: NSLayoutAttribute.LeftMargin,
            multiplier: 1.0,
            constant: 0.0);
        
        var labelHeightConstraint = NSLayoutConstraint(item: self.label,
            attribute: NSLayoutAttribute.Height,
            relatedBy: NSLayoutRelation.Equal,
            toItem: self,
            attribute: NSLayoutAttribute.Height,
            multiplier: 1.0,
            constant: 1.0);
        
        var labelLeftSticky = NSLayoutConstraint(item: self.label,
            attribute: NSLayoutAttribute.Left,
            relatedBy: NSLayoutRelation.Equal,
            toItem: self.imageView,
            attribute: NSLayoutAttribute.LeftMargin,
            multiplier: 1.0,
            constant: 5.0);
        
        self.addConstraint(imgHeightConstraint)
        self.addConstraint(imgLeftSticky)
        self.addConstraint(labelHeightConstraint)
        self.addConstraint(labelLeftSticky)
    }
    
}
