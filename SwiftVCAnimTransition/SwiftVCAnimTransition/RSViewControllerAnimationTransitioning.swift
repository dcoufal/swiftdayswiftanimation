//
//  RSViewControllerAnimationTransitioning.swift
//  SwiftVCAnimTransition
//
//  Created by David Coufal on 8/11/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

import UIKit

let kRSViewControllerAnimationTransitionDuration: NSTimeInterval = 0.5

func viewControllerAnimationTransitionFactory( fromVC: UIViewController, toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    var sourceProtocol = fromVC as? RSViewControllerAnimationSourceProtocol
    var targetProtocol = toVC as? RSViewControllerAnimationTargetProtocol
    
    if (sourceProtocol != nil && targetProtocol != nil) {
        if (sourceProtocol!.acceptsTargetClass(targetProtocol!.targetClass()) && targetProtocol!.acceptsSourceClass(sourceProtocol!.sourceClass())) {
            var transitioning = RSViewControllerAnimationTransitioning()
            
            transitioning.sourceDelegate = sourceProtocol
            transitioning.targetDelegate = targetProtocol
            
            return transitioning
        }
    }
    return nil;
}

class RSViewControllerAnimationTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    var sourceDelegate: RSViewControllerAnimationSourceProtocol?
    var targetDelegate: RSViewControllerAnimationTargetProtocol?
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning!) {
        if (self.sourceDelegate == nil || self.targetDelegate == nil) {
            return;
        }
        
        var fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        var toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        var container = transitionContext.containerView()
        
        var animatedView = self.sourceDelegate!.sourceView()
        var sourceRect = self.sourceDelegate!.sourceAnimationFrame()
        var targetRect = self.targetDelegate!.targetAnimationFrame()
        
        self.sourceDelegate?.willStartSourceAnimation?()
        self.targetDelegate?.willStartTargetAnimation?()
        
        container.addSubview(toVC.view)
        container.addSubview(animatedView)
        
        var duration = self.transitionDuration(transitionContext)
        var springDamping: CGFloat = 1.0
        if let delegateSpringDamping = self.sourceDelegate?.transitionSpringDamping?() {
            springDamping = delegateSpringDamping
        }
        var springVelocity: CGFloat = 0.0
        if let delegateSpringVelocity = self.sourceDelegate?.transitionSpringInitialVelocity?() {
            springVelocity = delegateSpringVelocity
        }
        
        UIView.animateWithDuration(
            duration,
            delay: 0.0,
            usingSpringWithDamping: springDamping,
            initialSpringVelocity: springVelocity,
            options: UIViewAnimationOptions.CurveEaseInOut,
            animations: { ()
                self.sourceDelegate?.preAnimationSetup(animatedView, sourceViewController: fromVC, targetViewController: toVC)
                animatedView.layoutIfNeeded()
            },
            completion: nil)

        UIView.animateWithDuration(
            duration,
            delay: 0.0,
            usingSpringWithDamping: springDamping,
            initialSpringVelocity: springVelocity,
            options: UIViewAnimationOptions.CurveEaseInOut,
            animations: { ()
                self.sourceDelegate?.postAnimationSetup(animatedView, sourceViewController: fromVC, targetViewController: toVC)
                animatedView.layoutIfNeeded()
            },
            completion: { (finished: Bool) in
                self.sourceDelegate?.didEndSourceAnimation?()
                self.targetDelegate?.didEndTargetAnimation?()
                
                animatedView.removeFromSuperview()
                transitionContext.completeTransition(finished)
            })
}

    func transitionDuration(transitionContext: UIViewControllerContextTransitioning!) -> NSTimeInterval {
        if (self.sourceDelegate != nil) {
            var sourceRect = self.sourceDelegate?.sourceAnimationFrame()
            var targetRect = self.targetDelegate?.targetAnimationFrame()
            var windowRect = UIScreen.mainScreen().bounds;
            var percentOfScreenTravelled = fabs(CGRectGetMidY(targetRect!) - CGRectGetMidY(sourceRect!)) / CGRectGetHeight(windowRect)
            percentOfScreenTravelled = max(min(percentOfScreenTravelled, 1.0), 0.1)
            
            if let duration = self.sourceDelegate?.transitionDuration?(percentOfScreenTravelled) {
                return duration
            }
        }
        return kRSViewControllerAnimationTransitionDuration
    }
    
}
