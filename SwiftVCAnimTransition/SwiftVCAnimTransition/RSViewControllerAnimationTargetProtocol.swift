//
//  RSViewControllerAnimationTargetProtocol.swift
//  SwiftVCAnimTransition
//
//  Created by David Coufal on 8/11/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

import UIKit

@objc protocol RSViewControllerAnimationTargetProtocol {
    func acceptsSourceClass( target: String) -> Bool
    func targetClass() -> String
    func targetAnimationFrame() -> CGRect
    optional func willStartTargetAnimation()
    optional func didEndTargetAnimation()
}
