//
//  RSSwVCAnimDataItem.swift
//  SwiftVCAnimTransition
//
//  Created by David Coufal on 8/11/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

import UIKit

class RSSwVCAnimDataItem: NSObject {
   
    var imageName: String = ""
    var shortText: String = "SAMPLE SHORT TEXT"
    var longText: String = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    var backgroundColor: UIColor = UIColor.clearColor()
    
    convenience init(imageName: String, shortText: String, longText:String, backgroundColor: UIColor) {
        self.init()
        self.imageName = imageName;
        self.shortText = shortText
        self.longText = longText;
        self.backgroundColor = backgroundColor;
    }
    
}
