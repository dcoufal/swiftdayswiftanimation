//
//  RSSwVCAnimDataSource.swift
//  SwiftVCAnimTransition
//
//  Created by David Coufal on 8/11/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

import UIKit

class RSSwVCAnimDataSource: NSObject {
    
    class func retrieveCollectionData() -> [RSSwVCAnimDataItem] {
        var data = Array<RSSwVCAnimDataItem>()
        
        var funFactYoungSnakes = RSSwVCAnimDataItem(
            imageName: "youngsnakes",
            shortText: "First Band",
            longText: "The Young Snakes were an American band formed in Boston in the early 1980s.\n\rLead singer and bassist Aimee Mann formed the group after she dropped out of Berklee College of Music along with guitarist/singer Doug Vargas and drummer Dave Bass Brown. Brown left the band in the fall of 1981 to help form the Boston hardcore punk band Negative FX, and was replaced by former D Club drummer Mike Evans.\n\rAfter releasing the song \"Brains and Eggs\" on the Modern Method compilation A Wicked Good Time, the band released a five-song EP, 1982’s Bark Along with The Young Snakes, on Ambiguous Records. The compilation album Aimee Mann & The Young Snakes, released in 2004, included \"Brains and Eggs\" and a radio performance, but not the Bark Along tracks.\n\rMann has described the band as \"a little punk noise-art outfit,\" noting that the group’s \"break every rule\" ethos became a rule in itself, and that her later band ’Til Tuesday was a rebellion against The Young Snakes’ lack of interest in \"sweetness and melody.\"",
            backgroundColor: UIColor(red: 1.0, green: 130.0/255.0, blue: 253.0/255.5, alpha: 1.0));
        
        var funFactTilTuesday = RSSwVCAnimDataItem(
            imageName: "tiltuesday",
            shortText: "'Til Tuesday",
            longText: "'Til Tuesday (often written as ’til tuesday) was an American new wave band formed in Boston in 1982. Its original lineup was bassist/vocalist Aimee Mann, guitarist/vocalist Robert Holmes, keyboardist Joey Pesce, and drummer Michael Hausman. They are best known for their top 10 smash \"Voices Carry\".\n\rThe \"Voices Carry\" single peaked at number eight on the U.S. Billboard Hot 100, and is said to have been inspired by an argument between Mann and Hausman, who had broken off a relationship before the album's release. According to producer Mike Thorne in his Stereo Society web site, \"the title track was originally written and sung by Aimee as if to a woman.... The record company was predictably unhappy with such lyrics.\" Rolling Stone magazine would later report that Epic Records labelmate Cyndi Lauper was interested in recording \"Voices Carry\" with the original lyric, but only if the band didn't put it on their own release. The band declined.\n\rThe band became an early MTV staple with the \"Voices Carry\" video, which depicts an oppressive boyfriend trying to convert Mann to his upper-class lifestyle; she finally lashes out at him during a concert at Carnegie Hall, though filmed at the Strand Theatre in Dorchester Massachusetts, standing up from her seat in the audience and belting the lyrics, \"He said, shut up! He said, shut up! Oh God, can't you keep it down...?\" as she removes her cap to reveal her signature spiky, rat-tailed hair. As a result, the group won that year's MTV Video Music Award for Best New Artist.",
            backgroundColor: UIColor(red: 169.0/255.0, green: 119.0/255.0, blue: 232.0/255.0, alpha: 1.0));
        
        var funFactMagnolia = RSSwVCAnimDataItem(
            imageName: "magnolia",
            shortText: "Magnolia",
            longText: "Paul Thomas Anderson, director of the film Magnolia, met Aimee Mann in 1996 when he asked her husband, Michael Penn, to write songs for his film, Hard Eight. Mann had songs on soundtracks before but never \"utilized in such an integral way\" she said in an interview. She gave Anderson rough mixes of songs and found that they both wrote about the same kinds of characters. He encouraged her to write songs for the film by sending her a copy of the script.\n\rTwo songs were written expressly for the film: \"You Do,\" which was based on a character later cut from the film, and \"Save Me,\" which closes the film; the latter was nominated in the 2000 Academy Awards and Golden Globes and in the 2001 Grammies. Most of the remaining seven Mann songs were demos and works in progress; \"Wise Up,\" which is at the center of a sequence in which all of the characters sing the song, was originally written for the 1996 film Jerry Maguire. At the time Mann's record label had refused to release her songs on an album. The song that plays at the opening of the film is Mann's cover of \"One\" by Harry Nilsson. Mann's track \"Momentum\" is used as the loud playing music in Claudia's apartment scene when Officer Jim arrives and was also featured in the trailer for the film.",
            backgroundColor: UIColor(red: 143.0/255.0, green: 155.0/255.0, blue: 1.0, alpha: 1.0));
        
        var funFactBigLebowski = RSSwVCAnimDataItem(
            imageName: "lebowski",
            shortText: "The Big Lebowski",
            longText: "In 1998, Mann appeared in the film The Big Lebowski as a German nihilist who sacrificed her green nail polished right little toe.",
            backgroundColor: UIColor(red: 119.0/255.0, green: 192.0/255.0, blue: 232.0/255.5, alpha: 1.0));
        
        var funFactTheBoth = RSSwVCAnimDataItem(
            imageName: "theboth",
            shortText: "The Both at the Bluebird",
            longText: "Aimee Mann's current band, The Both, performed at the Bluebird Theatre on Saturday night. Dave enjoyed himself.",
            backgroundColor: UIColor(red: 175.0/255.0, green: 1.0, blue: 239.0/255.0, alpha: 1.0));
        
        data.insert(funFactTheBoth, atIndex: 0)
        data.insert(funFactBigLebowski, atIndex: 0)
        data.insert(funFactMagnolia, atIndex: 0)
        data.insert(funFactTilTuesday, atIndex: 0)
        data.insert(funFactYoungSnakes, atIndex: 0)
        
        return data
    }
   
}
