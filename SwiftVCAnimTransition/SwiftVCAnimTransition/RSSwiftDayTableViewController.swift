//
//  RSSwiftDayTableViewController.swift
//  SwiftDaySwiftAnimation2
//
//  Created by David Coufal on 8/11/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

import UIKit

class RSSwiftDayTableViewController: UITableViewController, UIViewControllerTransitioningDelegate, RSViewControllerAnimationSourceProtocol, UINavigationControllerDelegate {
    
    var data: Array<RSSwVCAnimDataItem>? = nil
    var selectedCellCache: UITableViewCell? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.data = RSSwVCAnimDataSource.retrieveCollectionData();
        
        self.title = "Fun Facts about Aimee Mann"
    }
    
    // MARK: - Table View
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data!.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as RSSwVCAnimTableViewCell
        
        cell.alpha = 1.0
        
        let fact = self.data![indexPath.row] as RSSwVCAnimDataItem
        cell.headerView!.backgroundColor = fact.backgroundColor
        cell.headerView!.imageView!.image = UIImage(named: fact.imageName)
        cell.headerView!.label!.text = fact.shortText
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        cell.backgroundColor = fact.backgroundColor
        return cell
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    func selectedCell() -> UITableViewCell {
        var selectedPath = self.tableView.indexPathForSelectedRow()
        return self.tableView.cellForRowAtIndexPath(selectedPath)
    }
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var detailVC = storyboard.instantiateViewControllerWithIdentifier("DetailViewController") as DetailViewController
        detailVC.modalPresentationStyle = UIModalPresentationStyle.Custom
        detailVC.transitioningDelegate = self;
        detailVC.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
        let indexPath = self.tableView.indexPathForSelectedRow()
        let fact = self.data![indexPath.row] as RSSwVCAnimDataItem
        detailVC.detailItem = fact
        self.navigationController.delegate = self
        self.navigationController.pushViewController(detailVC, animated: true)
//        self.presentViewController(detailVC, animated: true, completion: nil)
    }
    
    // MARK: - Nav Controller Delegate
    
    func navigationController(navigationController: UINavigationController!, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController!, toViewController toVC: UIViewController!) -> UIViewControllerAnimatedTransitioning! {
        return viewControllerAnimationTransitionFactory(fromVC, toVC);
    }
 
    // MARK: - RSViewControllerAnimationSourceProtocol
    
    func acceptsTargetClass( target: String) -> Bool {
        return (target == "DetailViewController")
    }
    
    func sourceClass() -> String {
        return "MasterViewController"
    }
    
    func sourceView() -> UIView {
        var selectedPath = self.tableView.indexPathForSelectedRow()
        var selectedCell = self.selectedCell()
        
        var animatedView = RSSwVCAnimDataHeaderView(frame: selectedCell.frame)
        
        let fact = self.data![selectedPath.row] as RSSwVCAnimDataItem
        animatedView.backgroundColor = fact.backgroundColor
        animatedView.imageView!.image = UIImage(named: fact.imageName)
        animatedView.label!.text = fact.shortText
        
        return animatedView
    }
    
    func sourceAnimationFrame() -> CGRect {
        var selectedCell = self.selectedCell()
        
        return self.view.superview!.convertRect(selectedCell.frame, fromView: selectedCell.superview!)
    }
    
    func preAnimationSetup( view: UIView, sourceViewController: UIViewController, targetViewController: UIViewController) {
        sourceViewController.view.alpha = 0.0
        targetViewController.view.alpha = 0.0
    }
    
    func postAnimationSetup( view: UIView, sourceViewController: UIViewController, targetViewController: UIViewController) {
        sourceViewController.view.alpha = 0.0
        targetViewController.view.alpha = 1.0
    }
    
    func transitionDuration( distanceTravelledAsFractionOfScreen: CGFloat) -> NSTimeInterval {
        return 2.0
        //return 0.5 * (Double(distanceTravelledAsFractionOfScreen) + 0.3)
    }
    
    func transitionSpringDamping() -> CGFloat {
        return 0.75;
    }
    
    func transitionSpringInitialVelocity() -> CGFloat {
        return 0.4;
    }
    
    func willStartSourceAnimation() {
        var selectedCell = self.selectedCell()
        selectedCell.alpha = 0.0
        selectedCellCache = selectedCell
    }
    
    func didEndSourceAnimation() {
        self.selectedCellCache?.alpha = 1.0
        self.selectedCellCache = nil
        self.view.alpha = 1.0
    }
    
    // MARK: - UIViewControllerTransitioningDelegate
    
    func animationControllerForDismissedController(dismissed: UIViewController!) -> UIViewControllerAnimatedTransitioning! {
        return nil
    }
    
    func animationControllerForPresentedController(presented: UIViewController!, presentingController presenting: UIViewController!, sourceController source: UIViewController!) -> UIViewControllerAnimatedTransitioning! {
        return viewControllerAnimationTransitionFactory(source, presented);
    }
}
